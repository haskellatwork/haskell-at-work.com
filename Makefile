GENERATOR=$(shell stack path --local-install-root)/bin/haskell-at-work-generator
SITE=site/_site
CLOUDFRONT_DISTRIBUTION_ID=E1ERSPDM2ENU5L
NPM_BIN=$(shell npm bin)

watch:
	cd site && PATH=$(NPM_BIN):$(PATH) cabal new-run haskell-at-work-generator watch

deploy:
	cd site && PATH=$(NPM_BIN):$(PATH) cabal new-run haskell-at-work-generator build
	aws s3 sync site/_site/ s3://haskell-at-work.com
	aws cloudfront create-invalidation --distribution-id $(CLOUDFRONT_DISTRIBUTION_ID) --paths '/*'

clean:
	cd site && cabal new-run haskell-at-work-generator clean
