# Haskell at Work Website

This is the source code and content for https://haskell-at-work.com. All source
code in `generator/` and the Markdown and HTML templates in `site/` is licensed
under [The Unlicense](UNLICENSE).

The content, everything under `site/episodes` is "All Rights Reserved".

Copyright 2017 Oskar Wickström
