{ pkgs ? import <nixpkgs> {} }:
let
  generator = pkgs.haskellPackages.callPackage (import ./haskell-at-work.nix) {};
  site = pkgs.stdenv.mkDerivation {
     name = "haskell-at-work-site";
     src = ./site ;
     phases = "unpackPhase buildPhase";
     buildInputs = [ generator pkgs.yuicompressor pkgs.lessc ];
     buildPhase = ''
       export LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
       export LANG=en_US.UTF-8
       haskell-at-work-generator build
       mkdir "$out"
       cp -r _site/* "$out"
     '';
   };
   shell = pkgs.haskellPackages.shellFor {
    withHoogle = true;
    packages = p: [generator];
    buildInputs = with pkgs; [
      nodejs
      nodePackages.clean-css
      nodePackages.less-plugin-clean-css
      lessc
    ];
  };
in
  if pkgs.lib.inNixShell then shell else site
