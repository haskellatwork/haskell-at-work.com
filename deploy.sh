#!/usr/bin/env bash

SITE=$(nix-build --no-out-link)
rsync -av "$SITE/" ubuntu@wickstrom.tech:sites/haskell-at-work --delete
aws s3 sync "$SITE/" s3://haskell-at-work.com
