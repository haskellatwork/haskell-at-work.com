{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Data.List                        (isPrefixOf)
import           Data.Maybe                       (fromMaybe)
import           Data.Monoid                      (mappend)
import           Hakyll
import           Text.Pandoc                      (Format (..), Pandoc,
                                                   writerHighlightStyle)
import           Text.Pandoc.Filter.EmphasizeCode (emphasizeCode)
import           Text.Pandoc.Walk                 (walkM)

data NavItem = NavItem
  { navItemTitle :: String
  , navItemUrl   :: String
  }

config :: Configuration
config = defaultConfiguration

addSiteVerification :: Rules ()
addSiteVerification =
  match "google3f5ca0727b7e1c50.html" $ do
    route idRoute
    compile copyFileCompiler

lessRules :: Rules ()
lessRules = do
  match "less/*.less" (compile getResourceBody)
  -- Compile the main less file.
  create ["css/style.css"] $ do
    route idRoute
    compile $
      -- We tell hakyll it depends on all the less files, so it will recompile
      -- it when needed.
     do
      getMatches "less/*.less"
      loadBody "less/main.less" >>=
        unixFilter
          "lessc"
          ["-", "--clean-css", "--include-path=less", "-x"] >>=
        makeItem

emphasizeCodeTransform :: Pandoc -> IO Pandoc
emphasizeCodeTransform = walkM (emphasizeCode (Just (Format "html5")))

customPandocCompiler :: Compiler (Item String)
customPandocCompiler =
  pandocCompilerWithTransformM
    defaultHakyllReaderOptions
    defaultHakyllWriterOptions {writerHighlightStyle = Nothing}
    (unsafeCompiler . emphasizeCodeTransform)

main :: IO ()
main =
  hakyllWith config $ do
    match "images/*" $ do
      route idRoute
      compile copyFileCompiler
    lessRules
    match "assets/**/*" $ do
      route idRoute
      compile copyFileCompiler
    addSiteVerification
    match (fromList ["about.md", "error.md"]) $ do
      route $ setExtension "html"
      compile $
        pandocCompiler >>= loadAndApplyTemplate "templates/default.html" pageCtx
    match "episodes/*" $ do
      route $ setExtension "html"
      compile $
        customPandocCompiler >>=
        loadAndApplyTemplate "templates/episode.html" episodeCtx >>=
        saveSnapshot "content" >>=
        loadAndApplyTemplate "templates/default.html" episodeCtx
    create ["episodes.html"] $ do
      route idRoute
      compile $ do
        episodes <- recentFirst =<< loadAll "episodes/*"
        let episodesCtx =
              listField "episodes" episodeCtx (return episodes) `mappend`
              constField "title" "Episodes" `mappend`
              pageCtx
        makeItem "" >>=
          loadAndApplyTemplate "templates/episodes.html" episodesCtx >>=
          loadAndApplyTemplate "templates/default.html" episodesCtx
    match "index.html" $ do
      route idRoute
      compile $ do
        episodes <- fmap (take 4) <$> recentFirst =<< loadAll "episodes/*"
        let indexCtx =
              listField "episodes" episodeCtx (return episodes) `mappend`
              siteCtx
        getResourceBody >>= applyAsTemplate indexCtx >>=
          loadAndApplyTemplate "templates/default.html" indexCtx
    create ["atom.xml"] (route idRoute >> compileFeed renderAtom)
    create ["feed.xml"] (route idRoute >> compileFeed renderRss)
    match "templates/*" $ compile templateBodyCompiler

compileFeed rendered =
  compile $ do
    let feedCtx = episodeCtx `mappend` bodyField "description"
    episodes <- recentFirst =<< loadAllSnapshots "episodes/*" "content"
    rendered myFeedConfiguration feedCtx episodes

siteCtx :: Context String
siteCtx = navItemsCtx `mappend` defaultContext
  where
    pageNavClass args item = do
      route <- getRoute (itemIdentifier item)
      case args of
        [url]
          | route == Just url -> return "active"
          | otherwise -> return mempty
    navItemsCtx =
      listFieldWith "nav-items" navItemCtx (const (mapM makeItem navItems))
    navItemCtx :: Context NavItem
    navItemCtx =
      mconcat
        [ field "title" (return . navItemTitle . itemBody)
        , field "url" $ \item -> do
            let url = navItemUrl (itemBody item)
            r <- getRoute (fromFilePath url)
            case fromMaybe url r of
              "index.html" -> return "/"
              u
                | "http" `isPrefixOf` u -> return u
                | otherwise -> return ("/" ++ u)
        , field "class" $ \item -> do
            route <- getRoute =<< getUnderlying
            itemUrl <- getRoute (fromFilePath (navItemUrl (itemBody item)))
            if route == itemUrl
              then return "active"
              else return ""
        ]

episodeCtx :: Context String
episodeCtx =
  mconcat
    [ field "body-class" (const (pure "episode"))
    , dateField "date" "%B %e, %Y"
    , siteCtx
    ]

navItems =
  [ NavItem "Episodes" "episodes.html"
  , NavItem "About" "about.md"
  ]

pageCtx :: Context String
pageCtx = boolField "include-title" (const True) `mappend` siteCtx

myFeedConfiguration :: FeedConfiguration
myFeedConfiguration =
  FeedConfiguration
  { feedTitle = "Haskell at Work"
  , feedDescription = "A screencast focused on practical Haskell programming."
  , feedAuthorName = "Oskar Wickström"
  , feedAuthorEmail = "oskar.wickstrom@gmail.com"
  , feedRoot = "https://haskell-at-work.com"
  }
