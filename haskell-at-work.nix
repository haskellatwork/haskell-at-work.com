{ mkDerivation, base, hakyll, pandoc, pandoc-emphasize-code
, pandoc-types, stdenv
}:
mkDerivation {
  pname = "haskell-at-work";
  version = "0.2.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base hakyll pandoc pandoc-emphasize-code pandoc-types
  ];
  license = stdenv.lib.licenses.mpl20;
}
