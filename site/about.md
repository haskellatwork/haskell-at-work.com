---
title: About
---

*Haskell at Work* is a screencast focused on practical Haskell programming,
produced by [Oskar Wickström](https://wickstrom.tech). Viewers should have a
basic understanding of Haskell, and be eager to learn new ways of working
with Haskell.

The name has a double meaning; it's both about using Haskell for your
professional projects, as much as putting Haskell itself to work.

## FAQ

### "Is there any way of financially supporting the screencasts?"

Not anymore. I have decided to close down the Patreon due to the unhealthy
stress it gave me, a pressure to produce content regularly. I couldn't keep
that up with my full-time job and private life, so I decided to revert to
doing these screencasts without compensation.

If you'd like to pay for timely published Haskell material, I'd consider
subscribing to [Type Classes](https://typeclasses.com/).

### "Do you have a feed?"

You can use the [Atom feed](https://haskell-at-work.com/atom.xml) or the [RSS feed](https://haskell-at-work.com/feed.xml).

### "What editor are you using?"

In the videos so far, I'm using the following programs and plugins:

* [Neovim 0.2.x](https://github.com/neovim/neovim) editor
* [intero-neovim](https://github.com/parsonsmatt/intero-neovim) for
  GHCi-powered type checking, REPL, type inspection, and more
* [NeoMake](https://github.com/neomake/neomake) together with
  [hlint](https://hackage.haskell.org/package/hlint)
* NeoFormat, configured to use
    - hindent (you might also want to have a look at
        [brittany](https://github.com/lspitzner/brittany)), and
    - [stylish-haskell](https://github.com/jaspervdj/stylish-haskell).
* [vim-one](https://github.com/rakr/vim-one) colorscheme

You can dig around [the Neovim config in my
dotfiles](https://github.com/owickstrom/dotfiles/tree/master/config/nvim) to
see more details. All plugins are installed using
[Plug](https://github.com/junegunn/vim-plug).

## Feedback

If you have any suggestions for future topics, drop a line in the [Discussions
tab](https://www.youtube.com/channel/UCUgxpaK7ySR-z6AXA5-uDuw/discussion) in
the YouTube channel.

## Sponsors

A big thank you to [the patrons](https://www.patreon.com/haskellatwork) and
[Mpowered Business Solutions](http://mpowered.co.za) for helping me fund this
work!
